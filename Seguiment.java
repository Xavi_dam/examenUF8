package com.example.alumnedam.examenxavim8;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class Seguiment extends AppCompatActivity implements View.OnClickListener {

    Button local1, local2, local3, local11, local22, local33, vis1, vis2, vis3, vis11, vis22, vis33;
    EditText local, visitant;

    int puntsLocal=0, puntsVisitant=0;
    String puntsLoc, puntsVis;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.seguiment);

        local1 = (Button) findViewById(R.id.positiu1Local);
        local2 = (Button) findViewById(R.id.positiu2Local);
        local3 = (Button) findViewById(R.id.positiu3Local);
        local11 = (Button) findViewById(R.id.negatiu1Local);
        local22 = (Button) findViewById(R.id.negatiu2Local);
        local33 = (Button) findViewById(R.id.negatiu3Local);
        vis1 = (Button) findViewById(R.id.positiu1Visitant);
        vis2 = (Button) findViewById(R.id.positiu2Visitant);
        vis3 = (Button) findViewById(R.id.positiu3Visitant);
        vis11 = (Button) findViewById(R.id.negatiu1Visitant);
        vis22 = (Button) findViewById(R.id.negatiu2Visitant);
        vis33 = (Button) findViewById(R.id.negatiu3Visitant);

        local = (EditText) findViewById(R.id.puntsLocalDef);
        visitant = (EditText) findViewById(R.id.puntsVisitantDef);
        
        
    }


    @Override
    public void onClick(View view) {

        if(view.getId()==R.id.positiu1Local){
            puntsLocal++;
            local.setText(puntsLocal);
        }
        else if (view.getId()==R.id.positiu2Local){
            puntsLocal+=2;
            local.setText(puntsLocal);
        }
        else if (view.getId()==R.id.positiu3Local){
            puntsLocal+=3;
            local.setText(puntsLocal);
        }
        else if (view.getId()==R.id.negatiu1Local){
            puntsLocal-=1;
            local.setText(puntsLocal);
        }
        else if (view.getId()==R.id.negatiu2Local){
            puntsLocal-=2;
            local.setText(puntsLocal);
        }
        else if (view.getId()==R.id.negatiu3Local){
            puntsLocal-=3;
            local.setText(puntsLocal);
        }
        else if (view.getId()==R.id.positiu1Visitant){
            puntsVisitant++;
            local.setText(puntsVisitant);

        }else if (view.getId()==R.id.positiu2Visitant){
            puntsVisitant+=2;
            local.setText(puntsVisitant);
        }
        else if (view.getId()==R.id.positiu3Visitant){
            puntsVisitant+=3;
            local.setText(puntsVisitant);
        }
        else if (view.getId()==R.id.negatiu1Local){
            puntsVisitant-=1;
            local.setText(puntsVisitant);
        }
        else if (view.getId()==R.id.negatiu2Local){
            puntsVisitant-=2;
            local.setText(puntsVisitant);
        }
        else if (view.getId()==R.id.negatiu3Local) {
            puntsVisitant -= 3;
            local.setText(puntsVisitant);
        }
    }
}
