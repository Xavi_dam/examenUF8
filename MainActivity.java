package com.example.alumnedam.examenxavim8;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{

    EditText local, visitant, jornada;
    Button enviar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        local = (EditText) findViewById(R.id.local);
        visitant = (EditText) findViewById(R.id.visitant);
        jornada= (EditText) findViewById(R.id.jornada);
        enviar = (Button) findViewById(R.id.enviar);

    }

    @Override
    public void onClick(View view) {

            Intent data = new Intent(MainActivity.this, Seguiment.class);
            data.putExtra("equipLocal", local.getText().toString());
            data.putExtra("equipVisitant", visitant.getText().toString());
            data.putExtra("jornada", jornada.getText().toString());
            startActivity(data);




    }



}
